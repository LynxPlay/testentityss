package me.lynx.youtube;

import me.lynx.youtube.listener.DoubleJump;

import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;
import org.lynxplay.api.Chat;

public class Main extends JavaPlugin{

	private static Chat c;
	
	@Override
	public void onEnable() {
		c = new Chat();
		System.out.print("Das Plugin wurde geladen");
		registerListerner(getServer().getPluginManager());
		registerCommands();
	}

	private void registerCommands() {
		
	}

	private void registerListerner(PluginManager pm) {
		pm.registerEvents(new DoubleJump(), this);
	}
	
	public static Plugin getPlugin(){
		return getPlugin(Main.class);
	}
	
	public static Chat getChat(){
		return c;
	}

	
}
