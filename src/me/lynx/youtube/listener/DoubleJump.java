package me.lynx.youtube.listener;

import org.bukkit.GameMode;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.event.player.PlayerToggleFlightEvent;
import org.bukkit.util.Vector;

public class DoubleJump implements Listener {

	@EventHandler
	public void onMove (PlayerMoveEvent e){
		Player p = e.getPlayer();
		if(p.getGameMode() == GameMode.CREATIVE){
			return;
		}
		if(!p.hasPermission("doublejump.jump")){
			return;
		}
		Block below = p.getLocation().add(0, -1, 0).getBlock();
		if((below.getType() == Material.AIR || below.isLiquid()) && !p.isFlying()){
			return;
		}
		p.setAllowFlight(true);
	}
	
	@EventHandler
	public void onDoubleJump(PlayerToggleFlightEvent e){
		Player p = e.getPlayer();
		if(p.getGameMode() == GameMode.CREATIVE){
			return;
		}
		if(!p.hasPermission("doublejump.jump")){
			p.setAllowFlight(false);
			return;
		}
		e.setCancelled(true);
		p.setAllowFlight(false);
		Vector v = p.getLocation().getDirection();
		v.setY(1D);
		v.multiply(1.2D);
		p.setVelocity(v);
	}
	
}
